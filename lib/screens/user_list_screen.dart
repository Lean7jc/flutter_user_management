import 'package:flutter/material.dart';

import '/utils/api.dart';
import '/models/user.dart';
class UserListScreen extends StatefulWidget {
	@override
	UserListScreenState createState() => UserListScreenState();
}

class UserListScreenState extends State<UserListScreen> {
	Future? futureUsers;
	final refreshIndicatorKey = GlobalKey<RefreshIndicatorState>();

	List<Widget> generateListTiles(List<User> users) {
		List<Widget> listTiles = [];

		for (User user in users) {
			listTiles.add(ListTile(
				title: Text(user.name),
				subtitle: Text(user.email)
			));
		}

		return listTiles;
	}

	@override
	void initState() {
		super.initState();

		WidgetsBinding.instance!.addPostFrameCallback((timestamp) {
			setState(() {
				futureUsers = API().getUsers();
			});
		});
	}

	@override
	Widget build(BuildContext context) {
		Widget fbUserList = FutureBuilder(
			future: futureUsers,
			builder: (context, snapshot) {
				if (snapshot.connectionState == ConnectionState.done) {
					if (snapshot.hasError) {
						return Center(
							child: Text('Could not load the user list, restart the app.')
						);
					}

					return RefreshIndicator(
						key: refreshIndicatorKey,
						onRefresh: () async {
							setState(() {
								futureUsers = API().getUsers();
							});
						},
						child: ListView(
							padding: EdgeInsets.all(8.0),
							children: generateListTiles(snapshot.data as List<User>)
						)
					);
				}

				return Center(
					child: CircularProgressIndicator()
				);
			}
		);

		return Scaffold(
			 drawer: Drawer(
                child: ListView(
                    children: [
                        ListTile(
	                        title: Text('Add a user'),
							onTap: () {
							Navigator.push(
				              context,
				              MaterialPageRoute(builder: (context) => AddUserScreen()),
				            );
							}
                        )
                    ],
                )
            ),
			appBar: AppBar(title: Text('User List')),
			body: Container(
				child: fbUserList
			)
		);
	}
}


// Adding a new user
class AddUserScreen extends StatelessWidget {
  tfname = TextEditingController();
  final tfemail = TextEditingController();
  final tfphone = TextEditingController();
  final tfwebsite = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Add new user"),
      ),
      body: Container(
        child: ElevatedButton(
          onPressed: () {
	            Navigator.push(
	            	context,
	            	MaterialPageRoute(builder: (context) => UserListScreen()),
	            );
          	},
          child: Text('Go back!'),
        ),
      ),
    );
  }
  Widget tfname = TextFormField(
        decoration: InputDecoration(labelText: 'Name'),
    );
}